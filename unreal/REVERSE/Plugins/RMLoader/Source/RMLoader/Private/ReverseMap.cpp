#include "ReverseMap.h"

// Sets default values
AReverseMap::AReverseMap()
{

#ifdef Assemblage_TICK
	PrimaryActorTick.bCanEverTick = true;
#endif

}

AReverseMap::AReverseMap(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	root = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("root"));
	root->SetMobility(EComponentMobility::Movable);
	SetRootComponent(root);

#ifdef Assemblage_TICK
	PrimaryActorTick.bCanEverTick = true;
#endif

}

// Called when the game starts or when spawned
void AReverseMap::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AReverseMap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AReverseMap::OnConstruction(const FTransform& transform)
{
	Super::OnConstruction(transform);
	update_bassins();
	update_materials();
}

void AReverseMap::AttachBassin(AReverseBassin* bassin) {

	if (bassins.Contains(bassin)) {
		return;
	}
	bassins.Add(bassin);
	bassin->AttachToComponent(this->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform, "bassin");

}

void AReverseMap::update_bassins() {

	TArray<USceneComponent*> cmps = this->GetRootComponent()->GetAttachChildren();
	for (int i = 0, imax = cmps.Num(); i < imax; ++i) {
		AActor* a = cmps[i]->GetOwner();
		if (a->GetClass()->IsChildOf(AReverseBassin::StaticClass())) {
			AttachBassin(Cast<AReverseBassin>(a));
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("AReverseMap::update_bassins, children: %i"), bassins.Num());

}

void AReverseMap::update_materials() {

	UE_LOG(LogTemp, Warning, TEXT("AReverseMap::update_materials"));
	for (int i = 0, imax = bassins.Num(); i < imax; ++i) {
		AReverseBassin* bassin = bassins[i];
		bassin->set_materials(TopMaterial, SideMaterial);
	}

}

void AReverseMap::SetTopMaterial(UMaterialInterface* mat) {
	TopMaterial = mat;
	update_materials();
}

void AReverseMap::SetSideMaterial(UMaterialInterface* mat) {
	SideMaterial = mat;
	update_materials();
}