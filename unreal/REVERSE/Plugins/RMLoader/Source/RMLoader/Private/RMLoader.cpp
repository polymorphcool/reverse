// Copyright Epic Games, Inc. All Rights Reserved.

#include "RMLoader.h"
#include "RMLoaderStyle.h"
#include "RMLoaderCommands.h"
#include "Misc/MessageDialog.h"
#include "ToolMenus.h"

static const FName RMLoaderTabName("RMLoader");

#define LOCTEXT_NAMESPACE "FRMLoaderModule"

void FRMLoaderModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FRMLoaderStyle::Initialize();
	FRMLoaderStyle::ReloadTextures();

	FRMLoaderCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FRMLoaderCommands::Get().PluginAction,
		FExecuteAction::CreateRaw(this, &FRMLoaderModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FRMLoaderModule::RegisterMenus));
}

void FRMLoaderModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FRMLoaderStyle::Shutdown();

	FRMLoaderCommands::Unregister();
}

void FRMLoaderModule::PluginButtonClicked()
{
	// Put your "OnButtonClicked" stuff here
	//FText DialogText = FText::Format(
	//						LOCTEXT("PluginButtonDialogText", "Add code to {0} in {1} to override this button's actions"),
	//						FText::FromString(TEXT("FRMLoaderModule::PluginButtonClicked()")),
	//						FText::FromString(TEXT("RMLoader.cpp"))
	//				   );
	//FMessageDialog::Open(EAppMsgType::Ok, DialogText);

	const void* ParentWindowWindowHandle = nullptr;
	IMainFrameModule& MainFrameModule = FModuleManager::LoadModuleChecked<IMainFrameModule>(TEXT("MainFrame"));
	const TSharedPtr<SWindow>& MainFrameParentWindow = MainFrameModule.GetParentWindow();
	if (MainFrameParentWindow.IsValid() && MainFrameParentWindow->GetNativeWindow().IsValid())
	{
		ParentWindowWindowHandle = MainFrameParentWindow->GetNativeWindow()->GetOSWindowHandle();
	}

	if (ParentWindowWindowHandle == nullptr) {
		return;
	}

	IDesktopPlatform* DesktopPlatform = FDesktopPlatformModule::Get();
	bool bOpened = false;
	TArray<FString> OutFileNames;
	if (DesktopPlatform)
	{
		auto DefaultPath = FEditorDirectories::Get().GetLastDirectory(ELastDirectory::GENERIC_OPEN);
		bOpened = DesktopPlatform->OpenFileDialog(
			ParentWindowWindowHandle,
			"Reverse Data Loader",
			DefaultPath,
			TEXT(""),
			"Reverse data desc|*.json",
			EFileDialogFlags::Multiple,
			OutFileNames
		);
	}

	bOpened = (OutFileNames.Num() > 0);

	for (int i = 0; i < OutFileNames.Num(); ++i) {
		load_json(OutFileNames[i]);
	}

}

bool FRMLoaderModule::load_json(const FString& path) {

	if (!FPaths::FileExists(path)) {
		UE_LOG(LogTemp, Error, TEXT("Reverse map NOT found: %s"), *path);
		return false;
	}

	// loading json content
	FString j_string;
	FFileHelper::LoadFileToString(j_string, *path);

	//Create a json object to store the information from the json string
	//The json reader is used to deserialize the json object later on
	TSharedPtr<FJsonObject> j_object = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<>> j_reader = TJsonReaderFactory<>::Create(j_string);
	bool deserialize_success = FJsonSerializer::Deserialize(j_reader, j_object);
	bool json_valid = j_object.IsValid();
	UReverseDataType data_type = identify_json(j_object);

	if (data_type != UReverseDataType::invalid && deserialize_success && json_valid) {

		FString folder_path;
		FString FilenamePart;
		FString ExtensionPart;
		FPaths::Split(path, folder_path, FilenamePart, ExtensionPart);

		// decompression depends of type of json
		switch (data_type)
		{
		case map_data:
			if (!load_map(j_string)) {
				UE_LOG(LogTemp, Error, TEXT("Failed to read data from: %s"), *path);
				return false;
			}
			break;
		default:
			break;

		}

	}
	else {

		UE_LOG(LogTemp, Error, TEXT("something went wrong when loading %s"), *path);
		if (data_type == UReverseDataType::invalid) {
			UE_LOG(LogTemp, Error, TEXT("this is an invalid json file"));
		}
		if (!deserialize_success) {
			UE_LOG(LogTemp, Error, TEXT("deserialize failed"));
		}
		if (!json_valid) {
			UE_LOG(LogTemp, Error, TEXT("json is not valid"));
		}
		return false;
	}

	UE_LOG(LogTemp, Warning, TEXT("Json %s succesfully loaded."), *path);

	return true;

}

UReverseDataType FRMLoaderModule::identify_json(TSharedPtr<FJsonObject> json) {

	UReverseDataType type = UReverseDataType::invalid;

	if (json->HasField("type")) {

		const FString str_type = json->GetStringField("type");

		if (str_type == "MAP_DATA") {
			type = UReverseDataType::map_data;
		}
		// other types detection...
		// validate fields are there
		switch (type)
		{
		case map_data:
			if (
				!json->HasField("bassin_barycenter") || 
				!json->HasField("bassins")
				) {
				type = UReverseDataType::invalid;
			}
			break;
		case invalid:
		default:
			break;

		}
	}

	return type;

}

bool FRMLoaderModule::load_map(const FString& json_str) {

	FRerverseMap data_map;
	if (!FJsonObjectConverter::JsonObjectStringToUStruct<FRerverseMap>(json_str, &data_map, 0, 0)) {
		UE_LOG(LogTemp, Error, TEXT("Failed to deserialise FRerverseMap from json."));
		return false;
	}

	// trying to get current world
	UWorld* world = GEditor->EditorWorld;
	world = GEditor->GetEditorWorldContext().World();
	if (world == nullptr) {
		UE_LOG(LogTemp, Error, TEXT("world NOT found"));
		return false;
	}

	AReverseMap* map = world->SpawnActor<AReverseMap>(AReverseMap::StaticClass());
	map->SetActorLocation(RMUtils::load_vector(data_map.bassin_barycenter));
	map->SetActorLabel("RMap_" + FString::FromInt(data_map.unix_timestamp));

	UE_LOG(LogTemp, Error, TEXT("bassins num: %i"), data_map.bassins.Num());

	for (int i = 0, imax = data_map.bassins.Num(); i < imax; ++i) {

		const FRerverseMapBassin& bassin_data = data_map.bassins[i];

		//const FTransform SpawnLocAndRotation;
		//auto bassin = Cast<AReverseBassin>(UGameplayStatics::BeginDeferredActorSpawnFromClass(world, AReverseBassin::StaticClass(), SpawnLocAndRotation));
		//if (bassin != nullptr) {
		//	bassin->load_data(data_map.bassins[i]);
		//	UGameplayStatics::FinishSpawningActor(bassin, SpawnLocAndRotation);
		//}

		AReverseBassin* bassin = world->SpawnActor<AReverseBassin>(AReverseBassin::StaticClass());
		map->AttachBassin(bassin);
		if (!bassin->load_data(data_map.bassins[i])) {
			UE_LOG(LogTemp, Error, TEXT("Failed to load bassin %i"), i);
			bassin->K2_DestroyActor();
		}

	}

	return true;

}

void FRMLoaderModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FRMLoaderCommands::Get().PluginAction, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FRMLoaderCommands::Get().PluginAction));
				Entry.SetCommandList(PluginCommands);
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FRMLoaderModule, RMLoader)