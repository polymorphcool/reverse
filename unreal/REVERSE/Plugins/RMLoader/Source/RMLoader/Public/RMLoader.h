// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

// engine deps
#include "CoreMinimal.h"
#include "LocalizationTargetTypes.h"
#include "Developer/DesktopPlatform/Public/IDesktopPlatform.h"
#include "Developer/DesktopPlatform/Public/DesktopPlatformModule.h"
#include "Interfaces/IMainFrameModule.h" 
#include "EditorDirectories.h"
#include "JsonObjectConverter.h" 
#include "Misc/FileHelper.h" 
#include "Modules/ModuleManager.h"

// local deps
#include "RMCommon.h"
#include "ReverseBassin.h"
#include "ReverseMap.h"

class FToolBarBuilder;
class FMenuBuilder;

class FRMLoaderModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	
	/** This function will be bound to Command. */
	void PluginButtonClicked();
	
private:

	void RegisterMenus();

	bool load_json(const FString& path);

	bool load_map(const FString& json_str);

	UReverseDataType identify_json(TSharedPtr<FJsonObject> json);

	bool validate_map(const FString& path);


private:

	TSharedPtr<class FUICommandList> PluginCommands;

};
