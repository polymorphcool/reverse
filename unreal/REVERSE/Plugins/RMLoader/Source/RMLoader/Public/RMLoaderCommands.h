// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "RMLoaderStyle.h"

class FRMLoaderCommands : public TCommands<FRMLoaderCommands>
{
public:

	FRMLoaderCommands()
		: TCommands<FRMLoaderCommands>(TEXT("RMLoader"), NSLOCTEXT("Contexts", "RMLoader", "RMLoader Plugin"), NAME_None, FRMLoaderStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginAction;
};
