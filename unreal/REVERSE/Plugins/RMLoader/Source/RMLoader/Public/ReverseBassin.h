#pragma once

// engine deps
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h" 
#include "UObject/ConstructorHelpers.h"
#include "Materials/MaterialInstanceDynamic.h" 

// local deps
#include "RMCommon.h"
#include "ReverseBassin.generated.h"

//#define REVERSE_BASSIN_TICK

UCLASS()
class AReverseBassin : public AActor
{
	GENERATED_BODY()

private:

	FString display_mesh;
	FVector display_position;
	FString collision_mesh;
	FVector collision_position;

	FString display_name;
	FString tex_depth;
	FString tex_normal;

	//UMaterialInstanceDynamic* mat_top;
	//UMaterialInstanceDynamic* mat_side;
	UMaterialInterface* mat_top;
	UMaterialInterface* mat_side;

public:

	//AReverseBassin(const FRerverseMapBassin& bassin_data);
	
	// Sets default values for this actor's properties
	AReverseBassin();
	AReverseBassin(const FObjectInitializer& ObjectInitializer);

	UPROPERTY()
	class USceneComponent* root;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Reverse bassin")
	UStaticMeshComponent* base;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Reverse bassin")
	UStaticMeshComponent* visu01;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Reverse bassin")
	UStaticMeshComponent* visu02;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Reverse bassin")
	UStaticMeshComponent* visu03;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool load_data(const FRerverseMapBassin& bassin_data);

	void set_materials(UMaterialInterface* top, UMaterialInterface* side);

private:

	UStaticMeshComponent* init_static_mesh(const FName& name);

};
