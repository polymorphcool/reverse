#pragma once

#include "CoreMinimal.h"
#include "RMCommon.generated.h"

UENUM()
enum UReverseDataType
{
	invalid = 0, // default status
	map_data = 1, // when reception is ok
};

USTRUCT()
struct FRerverseMapBassin {
	GENERATED_USTRUCT_BODY()
	FRerverseMapBassin() {
		display_position.Add(0);
		display_position.Add(0);
		display_position.Add(0);
		collision_position.Add(0);
		collision_position.Add(0);
		collision_position.Add(0);
	}
	UPROPERTY()
	FString display_mesh;
	UPROPERTY()
	TArray<float> display_position;
	UPROPERTY()
	FString collision_mesh;
	UPROPERTY()
	TArray<float> collision_position;
	UPROPERTY()
	FString display_name;
	UPROPERTY()
	FString tex_depth;
	UPROPERTY()
	FString tex_normal;
};

USTRUCT()
struct FRerverseMap {
	GENERATED_USTRUCT_BODY()
	FRerverseMap() {
		bassin_barycenter.Add(0);
		bassin_barycenter.Add(0);
		bassin_barycenter.Add(0);
	}
	UPROPERTY()
	FString project;
	UPROPERTY()
	FString type;
	UPROPERTY()
	FString desc;
	UPROPERTY()
	int32 unix_timestamp;
	UPROPERTY()
	TArray<float> bassin_barycenter;
	UPROPERTY()
	TArray<FRerverseMapBassin> bassins;
};

class RMUtils {
public:
	static FVector load_vector(const TArray<float> arr) {
		FVector out;
		if (arr.Num() == 3) {
			out = FVector(arr[0], arr[1], arr[2]);
		}
		else {
			UE_LOG(LogTemp, Error, TEXT("vector decompression failed, %i"), arr.Num());
		}
		return out;
	}
};