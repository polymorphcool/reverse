#pragma once

// engine deps
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

// local deps
#include "ReverseBassin.h"
#include "ReverseMap.generated.h"

//#define Assemblage_TICK

UCLASS()
class AReverseMap : public AActor
{
	GENERATED_BODY()

private:
	TArray<AReverseBassin*> bassins;

public:

	// Sets default values for this actor's properties
	AReverseMap();
	AReverseMap(const FObjectInitializer& ObjectInitializer);

	UPROPERTY()
	class USceneComponent* root;

	void AttachBassin(AReverseBassin* bassin);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform& transform) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reverse map|display")
		UMaterialInterface* TopMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reverse map|display")
		UMaterialInterface* SideMaterial;

	UFUNCTION(BlueprintCallable, Category = "Reverse map Functions")
		void SetTopMaterial(UMaterialInterface* mat);

	UFUNCTION(BlueprintCallable, Category = "Reverse map Functions")
		void SetSideMaterial(UMaterialInterface* mat);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	void update_bassins();
	void update_materials();

};
