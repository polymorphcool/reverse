import bpy,math,mathutils,random

obj = bpy.context.object
mesh = bpy.context.object.data
UP = mathutils.Vector((0,0,1))
extrusion = [0.001,0.05]
candidates = []
current_candidate = None
max_count = 10

def store_polygon( poly ):
	candidates.append( [polygon.center[0],polygon.center[1]] )

def pick_random():
	global current_candidate
	if len(candidates) > 0:
		current_candidate = candidates[ int( len(candidates) * random.random() ) ]
		candidates.remove( current_candidate )
		return True
	return False

bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_all(action='DESELECT')
bpy.ops.object.mode_set(mode='OBJECT')

for polygon in mesh.polygons:
	# selecting only normal pointing up
	pnorm = mathutils.Vector(polygon.normal)
	if pnorm.dot( UP ) >= 0.99:
		#registration of polygon XY coordinates
		store_polygon( polygon )
		polygon.select = True
	else:
		polygon.select = False

# pick any candidate
run_count = 0
candidate_count = len(candidates)
while ( max_count == -1 or run_count < max_count ) and pick_random():
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
	bpy.ops.mesh.extrude_region_move(
		MESH_OT_extrude_region={
			"use_normal_flip":False, 
			"use_dissolve_ortho_edges":False, 
			"mirror":False
		}, 
		TRANSFORM_OT_translate={
			"value":(0, 0, extrusion[0]+random.random()*(extrusion[1]-extrusion[0])), 
			"orient_axis_ortho":'X', 
			"orient_type":'NORMAL', 
			"orient_matrix":((0.710627, -0.703569, 0), (0.703569, 0.710627, -0), (0, 0, 1)), 
			"orient_matrix_type":'NORMAL', "constraint_axis":(False, False, True), 
			"mirror":False, 
			"use_proportional_edit":False, 
			"proportional_edit_falloff":'SMOOTH', 
			"proportional_size":1, 
			"use_proportional_connected":False, 
			"use_proportional_projected":False, 
			"snap":False, 
			"snap_target":'CLOSEST', 
			"snap_point":(0, 0, 0), 
			"snap_align":False, 
			"snap_normal":(0, 0, 0), 
			"gpencil_strokes":False, 
			"cursor_transform":False, 
			"texture_space":False, 
			"remove_on_cancel":False, 
			"view2d_edge_pan":False, 
			"release_confirm":False, 
			"use_accurate":False, 
			"use_automerge_and_split":False
		})
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	
	pcount = 0
	ci = 0
	for c in candidates:
		for p in mesh.polygons:
			dx = abs( p.center[0] - c[0] )
			if dx > 0.001:
				continue
			dy = abs( p.center[1] - c[1] )
			if dy > 0.001:
				continue
			pnorm = mathutils.Vector(p.normal)
			if pnorm.dot( UP ) < 0.99:
				continue
			pcount += 1
			p.select = True
			break
		ci += 1
	
	run_count += 1
	if max_count > -1:
		print( run_count, '/', max_count, ', candidates: ', len(candidates) )
	else:
		print( abs(len(candidates)-candidate_count), '/', candidate_count )