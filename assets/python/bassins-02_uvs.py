import bpy, math, mathutils

'''
a UV unwrap form view will be performed by the script
'''

def getArea(type):
	for screen in bpy.context.workspace.screens:
		for area in screen.areas:
			if area.type == type:
				return area

def set_view_axis( t ): # 'TOP', 'FRONT', 'LEFT', ...
	global uv_override
	bpy.ops.view3d.view_selected(uv_override)
	bpy.ops.view3d.view_axis(uv_override, type=t)
	ns3d.region_3d.update()

def new_obj_bound( obj ):
	global obj_bounds
	if len( obj.data.vertices ) == 0:
		print( "unbounded object! ", obj.name )
		return None
	return {
		'obj': obj,
		'min': None,
		'max': None,
		'size': mathutils.Vector( [0,0,0] ),
		'center': mathutils.Vector( [0,0,0] ),
		'biggest_xy': -1
	}

def add_to_bound( bound, v ):
	
	if bound['min'] == None:
		bound['min'] = mathutils.Vector([ v.x, v.y, v.z ])
	else:	
		if bound['min'].x > v.x:
			bound['min'].x = v.x
		if bound['min'].y > v.y:
			bound['min'].y = v.y
		if bound['min'].z > v.z:
			bound['min'].z = v.z
	
	if bound['max'] == None:
		bound['max'] = mathutils.Vector([ v.x, v.y, v.z ])
	else:
		if bound['max'].x < v.x:
			bound['max'].x = v.x
		if bound['max'].y < v.y:
			bound['max'].y = v.y
		if bound['max'].z < v.z:
			bound['max'].z = v.z

def finish_bound( bound ):
	if bound['min'] != None and bound['max'] != None:
		bound['size'] = bound['max'] - bound['min']
		bound['center'] = bound['min'] + bound['size'] * 0.5
		bound['biggest_xy'] = bound['size'].x
		if bound['biggest_xy'] < bound['size'].y:
			bound['biggest_xy'] = bound['size'].y

def normalise_uv( uv_layer, uv_scale = 1, uv_offset = [0,0] ):
	bbox = None # [min x, min y, max x, max y]
	for el in uv_layer.data:
		if bbox == None:
			bbox = [ el.uv[0], el.uv[1], el.uv[0], el.uv[1] ]
		else:
			if bbox[0] > el.uv[0]:
				bbox[0] = el.uv[0]
			if bbox[1] > el.uv[1]:
				bbox[1] = el.uv[1]
			if bbox[2] < el.uv[0]:
				bbox[2] = el.uv[0]
			if bbox[3] < el.uv[1]:
				bbox[3] = el.uv[1]
	size = [ bbox[2]-bbox[0], bbox[3]-bbox[1] ]
	center = [ bbox[0] + size[0] * 0.5, bbox[1] + size[1] * 0.5 ]
	offset = [ 0.5 - center[0], 0.5 - center[1] ]
	scale = 1.0 / size[0]
	# proportionnality
	if size[1] > size[0]:
		scale = 1.0 / size[1]
	#centering and scaling UV
	for el in uv_layer.data:
		el.uv[0] = (((el.uv[0]-center[0]) * scale) + center[0] + offset[0])
		el.uv[1] = (((el.uv[1]-center[1]) * scale) + center[1] + offset[1])
		el.uv[0] = (((el.uv[0] - 0.5) * uv_scale) + 0.5 ) + uv_offset[0]
		el.uv[1] = (((el.uv[1] - 0.5) * uv_scale) + 0.5 ) + uv_offset[1]

grid_collection = bpy.data.collections['generated']

try:
	bpy.ops.object.mode_set(mode='OBJECT')
except:
	pass

# UV context related
prev_region = bpy.context.area.type
for ns3d in getArea('VIEW_3D').spaces:
	if ns3d.type == "VIEW_3D":
		break
areas3d  = [getArea('VIEW_3D')]
regions = [region for region in areas3d[0].regions if region.type == 'WINDOW']
uv_override = {'window':bpy.context.window,
			'screen':bpy.context.window.screen,
			'area'  :getArea('VIEW_3D'),
			'region':regions[0],
			'scene' :bpy.context.scene,
			'space' :getArea('VIEW_3D').spaces[0],
			}
if ns3d.region_3d.view_perspective == 'PERSP':
	bpy.ops.view3d.view_persportho(uv_override)

# registration of rows and columns positions
world_bound = None
obj_bounds = []
for obj in grid_collection.objects:
	# creating world bound
	if world_bound == None:
		world_bound = new_obj_bound( obj )
	else:
		world_bound['obj'] = obj
	# creating object bound
	ob = new_obj_bound( obj )
	for vert in obj.data.vertices:
		mat_loc = mathutils.Matrix.Translation( vert.co )
		mat_glob = obj.matrix_world @ mat_loc
		v = mat_glob.to_translation()
		add_to_bound( world_bound, v )
		add_to_bound( ob, v )
	# finishing object bound
	finish_bound( ob )
	obj_bounds.append( ob )

if world_bound != None:
	finish_bound( world_bound )
	
	ocount = 0
	for obound in obj_bounds:
		
		bpy.ops.object.select_all(action='DESELECT')
		obound['obj'].select_set(True)
		bpy.context.view_layer.objects.active = obound['obj']
		
		omesh = obound['obj'].data
		
		### UVS
		# brute force UV layers cleanup
		try:
			print( obound['obj'].name, " layers: ", len( omesh.uv_layers ) )
			while len( omesh.uv_layers ) > 0:
				print( obound['obj'].name, ' > ', omesh.uv_layers[0].name )
				omesh.uv_layers.remove( omesh.uv_layers[0] )
				omesh = obound['obj'].data
		except:
			pass
		
		## top world uv in first layer
		bpy.ops.mesh.uv_texture_add()
		uv_layer = omesh.uv_layers[-1]
		uv_layer.name = 'global'
		# create a top projection
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_mode(type='VERT')
		bpy.ops.mesh.select_all(action='SELECT')
		set_view_axis( 'TOP' )
		bpy.ops.uv.project_from_view( uv_override, camera_bounds=False, correct_aspect=True, scale_to_bounds=False )
		bpy.ops.object.mode_set(mode='OBJECT')
		uv_layer = omesh.uv_layers[-1]
		# and scale + translate relative to world
		uv_scale = obound['biggest_xy'] / world_bound['biggest_xy']
		uv_offset = [ 
			(obound['center'].x - world_bound['center'].x) / world_bound['biggest_xy'], 
			(obound['center'].y - world_bound['center'].y) / world_bound['biggest_xy']
		]
		normalise_uv( uv_layer, uv_scale = uv_scale	, uv_offset = uv_offset )
		
		## top local to bassin
		bpy.ops.mesh.uv_texture_add()
		uv_layer = omesh.uv_layers[-1]
		uv_layer.name = 'local'
		# create a top projection
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_mode(type='VERT')
		bpy.ops.mesh.select_all(action='SELECT')
		set_view_axis( 'TOP' )
		bpy.ops.uv.project_from_view( uv_override, camera_bounds=False, correct_aspect=True, scale_to_bounds=False )
		bpy.ops.object.mode_set(mode='OBJECT')
		uv_layer = omesh.uv_layers[-1]
		# fit to texture
		normalise_uv( uv_layer )
		
		## front projection bounded
		bpy.ops.mesh.uv_texture_add()
		uv_layer = omesh.uv_layers[-1]
		uv_layer.name = 'front'
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_mode(type='VERT')
		bpy.ops.mesh.select_all(action='SELECT')
		set_view_axis( 'FRONT' )
		bpy.ops.uv.project_from_view( uv_override, camera_bounds=False, correct_aspect=True, scale_to_bounds=True )
		bpy.ops.object.mode_set(mode='OBJECT')
		
		# create a standard projection
		bpy.ops.mesh.uv_texture_add()
		uv_layer = omesh.uv_layers[-1]
		uv_layer.name = 'smart'
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_mode(type='VERT')
		bpy.ops.mesh.select_all(action='SELECT')
		bpy.ops.uv.smart_project( uv_override )
		bpy.ops.object.mode_set(mode='OBJECT')
		
		ocount += 1
		print( obound['obj'].name, ' done, ', ocount , '/', len(obj_bounds) )