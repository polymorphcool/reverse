import bpy, json, os, time

'''
https://docs.blender.org/api/current/bpy.ops.export_scene.html
'''

SCALE = 100
SET_ORIGIN = True
EXPORT = True

export_folder = '//export/'
mesh_collection = 'generated'
collider_collection = 'collider'

property_blacklist = ['cycles']


b_data = { 
	'project': 'REVERSE',
	'type': 'MAP_DATA',
	'desc': 'this file is to be loaded by the RevereseMapLoader (RMLoader)',
	'unix_timestamp': int( time.time() ),
	'bassin_barycenter': [0,0,0],
	'bassins': []
}


def sync_xtra( src, dst ):
	global all_props
	for k in all_props.keys():
		dst[k] = all_props[k]['src']
		if type(dst[k]) is str:
			dst[k] = str('')
		elif type(dst[k]) is int:
			dst[k] = int(-1)
		elif type(dst[k]) is float:
			dst[k] = float(-1)
	
	# synchronising ones available in source
	for k in src.data.keys():
		if k in dst.keys():
			dst[k] = src.data[k]

try:
	bpy.ops.object.mode_set(mode='OBJECT')
except:
	pass

bpy.ops.object.select_all(action='DESELECT')

#### FOLDERS ####

export_folder = bpy.path.abspath( export_folder )
if not os.path.exists( export_folder ):
	os.makedirs( export_folder )

#### MESH ####

all_props = {}
for obj in bpy.data.collections[mesh_collection].objects:
	for k in obj.data.keys():
		if k == 'name' or k == 'position':
			print( 'custom properties named "name" or "position" are forbidden' )
			continue
		if k in property_blacklist:
			continue
		if not k in all_props:
			all_props[k] = {'src': obj.data[k] }

mesh_count = 0
for obj in bpy.data.collections[mesh_collection].objects:
	
	bpy.ops.object.select_all(action='DESELECT')
	obj.select_set(True)
	bpy.context.view_layer.objects.active = obj
	if SET_ORIGIN:
		bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
	
	name = obj.name.replace( '.', '_' )
	path = os.path.join( export_folder, name + '.fbx' )
	xtra = { 
		'display_mesh': name + '.fbx',
		'display_position' : [obj.location.x*SCALE,-obj.location.y*SCALE,obj.location.z*SCALE],
		'collision_mesh': '',
		'collision_position' : [0,0,0]
	}
	
	if EXPORT:
		loc = obj.location
		# move at 0 for export
		obj.location *= 0
		bpy.ops.export_scene.fbx( filepath = path, use_selection = True )
		# move back where i was
		obj.location = loc
	
	for i in range(0,3):
		b_data['bassin_barycenter'][i] += xtra['display_position'][i]
	sync_xtra( obj, xtra )
	b_data['bassins'].append( xtra )
	mesh_count += 1
	print( '\t\tmesh DONE! ', path )

for i in range(0,3):
	b_data['bassin_barycenter'][i] /= mesh_count

#### COLLIDER ####

collider_count = 0
for obj in bpy.data.collections[collider_collection].objects:
	
	bpy.ops.object.select_all(action='DESELECT')
	obj.select_set(True)
	bpy.context.view_layer.objects.active = obj
	if SET_ORIGIN:
		bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
	
	name = obj.name.replace( '.', '_' )
	path = os.path.join( export_folder, name + '.fbx' )
	
	# update data
	if collider_count < len(b_data['bassins']):
		b_data['bassins'][collider_count]['collision_mesh'] = name + '.fbx'
		b_data['bassins'][collider_count]['collision_position'] = [obj.location.x*SCALE,-obj.location.y*SCALE,obj.location.z*SCALE]
	
	if EXPORT:
		loc = obj.location
		# move at 0 for export
		obj.location *= 0
		bpy.ops.export_scene.fbx( filepath = path, use_selection = True )
		# move back where i was
		obj.location = loc
	
	collider_count += 1
	print( '\t\tcollider DONE! ', path )

json_file = os.path.join( export_folder, 'data.json' )
with open(json_file, 'w') as f:
	json.dump(b_data, f, ensure_ascii=False)

print( 'export finished, meshes:', mesh_count, ", colliders: ", collider_count )