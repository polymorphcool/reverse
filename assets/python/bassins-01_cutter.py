import bpy, math, mathutils

def sync_properties( src, dst ):
	global all_props
	
	# creating all custom properties
	bpy.ops.object.select_all(action='DESELECT')
	dst.select_set(True)
	bpy.context.view_layer.objects.active = dst
	for k in all_props.keys():
		dst.data[k] = all_props[k]['src']
		if type(dst.data[k]) is str:
			dst.data[k] = str('')
		elif type(dst.data[k]) is int:
			dst.data[k] = int(-1)
		elif type(dst.data[k]) is float:
			dst.data[k] = float(-1)
	
	# synchronising ones available in source
	for k in src.data.keys():
		if k in dst.data.keys():
			dst.data[k] = src.data[k]

def unlink_all_collections( obj ):
	bpy.ops.object.select_all(action='DESELECT')
	obj.select_set(True)
	bpy.context.view_layer.objects.active = obj
	bpy.ops.collection.objects_remove_all()

def name_obj( prefix, num ):
	if num > 999:
		print( 'name_obj ERROR!' )
		return None
	s = str(num)
	while len(s) < 3:
		s = '0' + s
	s = prefix + s
	for o in bpy.data.objects:
		if o.name == s:
			s = name_obj( prefix, num + 1 )
	return s

def belong_to_grid( x, y ):
	global gridx
	global gridy
	gx = int( x / GRID_TOLERANCE )
	gy = int( y / GRID_TOLERANCE )
	return gx in gridx and gy in gridy

TOLERANCE = 1e-5
GRID_TOLERANCE = 1e-4
grid_tmpl = bpy.data.objects['grid_tmpl']
grid_prefix = 'grid.'
cutter_collection = bpy.data.collections['cutters'] # boolean volumes
target_collection = bpy.data.collections['generated']

cutters = []
all_props = {}
for obj in cutter_collection.objects:
	cutters.append( obj )
	obj.data.name = obj.name
	for k in obj.data.keys():
		if not k in all_props:
			all_props[k] = {'src': obj.data[k] }

try:
	bpy.ops.object.mode_set(mode='OBJECT')
except:
	pass

# registration of rows and columns positions
gridx = []
gridy = []
for vert in grid_tmpl.data.vertices:
	vx = int( vert.co[0] / GRID_TOLERANCE )
	vy = int( vert.co[1] / GRID_TOLERANCE )
	if not vx in gridx:
		gridx.append( vx )
	if not vy in gridy:
		gridy.append( vy )
gridx = sorted( gridx )
gridy = sorted( gridy )

bcount = 0
for cutter in cutters:
	
	# selecting the template
	bpy.ops.object.select_all(action='DESELECT')
	grid_tmpl.select_set(True)
	bpy.context.view_layer.objects.active = grid_tmpl
	# copying
	bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False})
	res = bpy.context.view_layer.objects.active
	res.name = name_obj( grid_prefix, bcount )
	res.data.name = res.name
	
	# creation of all custom properties
	sync_properties( cutter, res )
	
	# cuting the bassin
	bpy.ops.object.modifier_add(type='BOOLEAN')
	mod = res.modifiers['Boolean']
	mod.object = cutter
	mod.operation = 'INTERSECT'
	bpy.ops.object.modifier_apply(modifier=mod.name)
	
	# marking sharp
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.mesh.select_mode(type='EDGE')
	bpy.ops.object.mode_set(mode='OBJECT')
	res_mesh = res.data
	res_mesh.name = res.name
	top_z = None
	bottom_z = None
	side_vertex_indices = []
	
	for e in res_mesh.edges:
		vert0 = res_mesh.vertices[ e.vertices[0] ]
		vert1 = res_mesh.vertices[ e.vertices[1] ]
		v0 = vert0.co
		v1 = vert1.co
		
		if top_z == None:
			top_z = v0[2]
		elif top_z < v0[2]:
			top_z = v0[2]
		elif top_z < v1[2]:
			top_z = v1[2]
		
		if bottom_z == None:
			bottom_z = v0[2]
		elif bottom_z > v0[2]:
			bottom_z = v0[2]
		elif bottom_z > v1[2]:
			bottom_z = v1[2]
		
		if v0[2] != v1[2]:
			# top > bottom all good!
			e.select = True
		elif belong_to_grid( v0[0], v0[1] ) or belong_to_grid( v1[0], v1[1] ):
			# vertices are aligned on one axis...
			e.select = False
		else:
			# all good also!
			e.select = True
		if e.select:
			if not vert0.index in side_vertex_indices:
				side_vertex_indices.append( vert0.index )
			if not vert1.index in side_vertex_indices:
				side_vertex_indices.append( vert1.index )
	
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.mark_sharp()
	bpy.ops.mesh.select_mode(type='VERT')
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# group with side vertices only
	side_vertex_indices = sorted(side_vertex_indices)
	group = bpy.context.object.vertex_groups.new()
	group.name = 'side'
	group.add(side_vertex_indices, 1.0, 'ADD')
	side_vertex_indices = [] # clear memory
	
	# group with top vertices only
	top_vertex_indices = []
	bottom_vertex_indices = []
	for v in res_mesh.vertices:
		if v.co[2] == top_z:
			top_vertex_indices.append( v.index )
		elif v.co[2] == bottom_z:
			bottom_vertex_indices.append( v.index )
	
	group = bpy.context.object.vertex_groups.new()
	group.name = 'top'
	group.add(top_vertex_indices, 1.0, 'ADD')
	top_vertex_indices = [] # clear memory
	
	group = bpy.context.object.vertex_groups.new()
	group.name = 'bottom'
	group.add(bottom_vertex_indices, 1.0, 'ADD')
	bottom_vertex_indices = [] # clear memory
	
	# add edge split modifier
	bpy.ops.object.modifier_add(type='EDGE_SPLIT')
	bpy.context.object.modifiers["EdgeSplit"].use_edge_angle = False
	
	# moving to target collection
	unlink_all_collections( res )
	target_collection.objects.link(res)
	
	bcount += 1
	
	print( bcount, '/', len(cutters), ' done, name: ', res.name )