import bpy, mathutils

def name_obj( prefix, num ):
	if num > 999:
		print( 'name_obj ERROR!' )
		return None
	s = str(num)
	while len(s) < 3:
		s = '0' + s
	s = prefix + s
	for o in bpy.data.objects:
		if o.name == s:
			s = name_obj( prefix, num + 1 )
	return s

DECIMATE_RATIO = 0.04
VERTEXGROUP_TOP = 'top'
VERTEXGROUP_BOTTOM = 'bottom'
OFFSET_TOP = None
OFFSET_BOTTOM = mathutils.Vector([0,0,0])
coll_prefix = 'coll.'
source_collection = bpy.data.collections['generated']
target_collection = bpy.data.collections['collider']

try:
	bpy.ops.object.mode_set(mode='OBJECT')
except:
	pass

bpy.ops.object.select_all(action='DESELECT')

for obj in target_collection.objects:
	obj.select_set(True)
bpy.ops.object.delete(use_global=False)

ccount = 0
for obj in source_collection.objects:
	
	bpy.ops.object.select_all(action='DESELECT')
	obj.select_set(True)
	bpy.context.view_layer.objects.active = obj
	origin_name = obj.name
	
	bpy.ops.object.duplicate_move(
		OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'},
		TRANSFORM_OT_translate={"value":(0, 0, 0)}
	)
	
	res = bpy.context.view_layer.objects.active
	res.name = name_obj( coll_prefix, ccount )
	res.data.name = res.name

	bpy.ops.collection.objects_remove(collection=source_collection.name)
	target_collection.objects.link(res)
	
	res.select_set(True)
	bpy.context.view_layer.objects.active = res
	
	while len( res.material_slots ) > 0:
		bpy.context.object.active_material_index = 0
		bpy.ops.object.material_slot_remove()
	
	while len( res.data.uv_layers ) > 0:
		bpy.ops.mesh.uv_texture_remove()
	
	while len( res.modifiers ) > 0:
		bpy.ops.object.modifier_remove(modifier=res.modifiers[0].name)
	
	bpy.ops.object.modifier_add(type='DECIMATE')
	decimate_mod = res.modifiers[-1]
	decimate_mod.ratio = DECIMATE_RATIO
	bpy.ops.object.modifier_apply(modifier=decimate_mod.name)
	
	topi = -1
	bottomi = -1
	for vg in res.vertex_groups:
		if vg.name == VERTEXGROUP_TOP:
			topi = vg.index
		elif vg.name == VERTEXGROUP_BOTTOM:
			bottomi = vg.index
	
	top_enabled = ( topi != -1 and OFFSET_TOP != None )
	bottom_enabled = ( bottomi != -1 and OFFSET_BOTTOM != None )
	if top_enabled or bottom_enabled:
		for v in res.data.vertices:
			for g in v.groups:
				if top_enabled and g.group == topi:
					v.co[0] += OFFSET_TOP.x
					v.co[1] += OFFSET_TOP.y
					v.co[2] += OFFSET_TOP.z
				elif bottom_enabled and g.group == bottomi:
					v.co[0] += OFFSET_BOTTOM.x
					v.co[1] += OFFSET_BOTTOM.y
					v.co[2] += OFFSET_BOTTOM.z
	
	bpy.ops.object.shade_flat()
	
	ccount += 1
	
	print( ccount, '/', len(source_collection.objects), ' done, name: ', res.name )