import json, os, time

SRC = 				'../untracked/Geojson/Tests/Threat/'
EXTENSION = 		'.geojson'
DIGITS = 			5
NAME_PREFIX = 		'data_'
DST = 				'../untracked/godot_ready_json/'

ERRORS = ''

for dirpath, dirs, files in os.walk(SRC):

	for f in files:
		
		if f[-len(EXTENSION):] == EXTENSION:
			
			dst_folder = os.path.join( DST, dirpath[len(SRC):], os.path.splitext(os.path.basename(f))[0] )
			
			
			try:
			
				# Opening JSON file
				jfile = open( os.path.join( dirpath, f ) , 'r')
				data = json.load(jfile)
				# json is loaded, time to create folder
				if not os.path.isdir( dst_folder ):
					os.makedirs( dst_folder )
				features = data['features']
				fcount = len(features)
				for i in range(0,fcount):
					fname = str(i)
					while len(fname) < DIGITS:
						fname = '0'+fname
					fname = NAME_PREFIX + fname + '.json'
					with open(os.path.join( dst_folder, fname ), 'w') as fjson:
						json.dump(features[i], fjson)
			
			except Exception as e:
			
				ERRORS += 'failed to process ' + os.path.join( dirpath, f ) + '\n'
				ERRORS += str(e) + '\n'

if len( ERRORS ) > 0:
	with open( os.path.join( DST, 'error_'+str(time.time())+'.log', ) , 'w' ) as f:
		f.write( ERRORS )
		f.close()
