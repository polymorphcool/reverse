tool

extends Spatial

export(String) var dir = ''
export(NodePath) var _ui:NodePath = ''
export(Material) var mat:Material = null
export(bool) var purge:bool = false setget set_purge
export(bool) var generate:bool = false setget set_generate

var ui_list:VBoxContainer = null
var jsons:Array = []
var meshes:Array = []

func set_generate(b:bool) -> void:
	generate = false
	if is_inside_tree() and b:
		recursive_scan( dir )

func is_data_file( fname:String ) -> bool:
	return fname.begins_with('data_') and fname.ends_with('.json')

func recursive_scan(path:String) -> void:
	var d:Directory = Directory.new()
	var subd:Directory = Directory.new()
	var data:Array = []
	if d.open(dir) == OK:
		d.list_dir_begin()
		var fn:String = d.get_next()
		while fn != "":
			if fn != '.' and fn != '..':
				var fullp:String = path + "/" + fn
				if subd.dir_exists( fullp ):
					recursive_scan( fullp )
				else:
					if is_data_file( fn ):
						data.append( fullp )
			fn = d.get_next()
	print( data )

func set_generate_old(b:bool) -> void:
	generate = false
	if is_inside_tree() and b:
		ui_list = get_node(_ui)
		var d:Directory = Directory.new()
		jsons = []
		meshes = []
		if d.open(dir) == OK:
			set_purge(true)
			d.list_dir_begin()
			var fn:String = d.get_next()
			while fn != "":
				if fn.ends_with( '.json' ):
					jsons.append( dir + fn )
				fn = d.get_next()
		jsons.sort()
		for j in jsons:
			load_json( j )
		
		for mesh in meshes:
			var mi:MeshInstance = MeshInstance.new()
			mi.mesh = load( mesh.path )
			mi.material_override = mat
			add_child( mi )
			mi.owner = self.owner
			if ui_list != null:
				var lbl:Label = Label.new()
				ui_list.add_child(lbl)
				lbl.owner = ui_list.owner
				lbl.text = mesh.name

func set_purge(b:bool) -> void:
	purge = false
	if is_inside_tree() and b:
		while get_child_count() > 0:
			remove_child( get_child(0) )
		ui_list = get_node(_ui)
		if ui_list != null:
			while ui_list.get_child_count() > 0:
				ui_list.remove_child( ui_list.get_child(0) )

func load_json( path:String ) -> void:
	
	var f:File = File.new()
	f.open( path, File.READ )
	var content:String = f.get_as_text()
	var data = parse_json( content )
	if data == null:
		print( "ERROR ", path )
	
	var st:SurfaceTool = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_LINES)
	var prev = null
	for vert in data.geometry.coordinates[0][0]:
		var v:Vector3 = Vector3( vert[0], 0, vert[1] )
		if prev != null:
			st.add_vertex(prev)
			st.add_vertex(v)
		prev = v
	
	var newm:Mesh = Mesh.new()
	st.commit( newm )
	
	var mpath:String = path.replace( '.json', '.mesh' )
	ResourceSaver.save( mpath, newm )
	
	var name:String = ''
	if data.properties.has( 'pais' ):
		name += str(data.properties.pais)
	if data.properties.has( 'nombre' ):
		name += ' // ' + str(data.properties.nombre)
	if data.properties.has( 'cia' ):
		name += ' // ' + str(data.properties.cia)
	
	meshes.append({
		'path': mpath,
		'name': name,
	})
	
#	var mi:MeshInstance = MeshInstance.new()
#	mi.mesh = newm
#	mi.material_override = mat
#	add_child( mi )
#	mi.owner = self.owner
#	mi.name = fname
#	if ui_list != null:
#		var lbl:Label = Label.new()
#		ui_list.add_child(lbl)
#		lbl.owner = ui_list.owner
#		if data.properties.has( 'pais' ):
#			lbl.text += str(data.properties.pais)
#		if data.properties.has( 'nombre' ):
#			lbl.text += ' // ' + str(data.properties.nombre)
#		if data.properties.has( 'cia' ):
#			lbl.text += ' // ' + str(data.properties.cia)
#		lbl.text += ' [' + fname + ']'
	
	print( "success ", path )
