# Rio Re/Verse

Mixed reality project based on Godot & Oculus Quest (tbc)

see:

- https://rivers.ulara.org/
- https://ulara.org/selected-work/visuals

## OCULUS installation

- créer compte développeur sur https://developer.oculus.com/

### androisd studio

- installer Android Studio 4.0
- aller dans "configuration
- - désactiver les updates dans "System settings > Updates"
- - dans "System settings > Android SDK > SDK Patforms", décocher tous et cocher **Android API 31**
- - dans "System settings > Android SDK > SDK Tools", cocher **Android SDK Command-line Tools**
- - dans "System settings > Android SDK > SDK Tools", décocher Hide obsolete dans le fond de la fenêtre
- - dans "System settings > Android SDK > SDK Tools", cocher **Android SDK Tools (Obsolete)**
- linker unreal and android avec C:\Program Files\Epic Games\UE_4.27\Engine\Extras\Android\SetupAndroid.bat

### once in unreal

- reconfigurer le projet unreal:
- - désactiver le addon steamVR
- - activer Oculus OpenXR
- - activer OpenXR
- - dans Launch, dropdwon > "devices", il faut voir Quest_2...

## pour se connecter

- mettre le casque en mode Oculus Link > menu principal dans la salle japonaise, cliquer sur le bouton à gauche et choisir Oculus Link
- une fois fait, Play dans l'éditor

## references

- How To Setup Unreal Engine 4.27 for Oculus Quest 2 Virtual Reality VR: https://y.com.sb/watch?v=XR2t_PMQbn4
- Using UE4 to Develop for Oculus Quest 2 |Quick-Start Guide: https://y.com.sb/watch?v=HgwXqRuEOo0
- Get Started with Oculus Quest 2 Development - Part 1: Oculus Developer Account: https://y.com.sb/watch?v=VI-8XJ2jw1s

## GEOJSON PARSING

### prepration

geojson are FAR too heavy to be processed by godot, even in editor

best solution: split them into tiny chunks and load them one by one	

to do so:

- download REVERSE ftp folder into **assets/untracked**
- `cd assets/python`
- `python3 feature_splitter.py`

once done, go to **assets/untracked/godot_ready_json**

all json are there, folder name match original geojson, 
each **data*.json** containing the data from one feature.

### loading

open godot project and open **res://geojson/geojson.tscn**

select node **root/geojson** in scene tab and press **generate**
